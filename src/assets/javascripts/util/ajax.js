var dummyFn = function(){};

var ajaxRequest = function(method, url, successFn, errorFn){
    var request;

    successFn = successFn || dummyFn;
    errorFn = errorFn || dummyFn;

    request = new XMLHttpRequest();
    request.open(method, url, true);

    request.onreadystatechange = function() {
        if (this.readyState === 4){
            if (this.status >= 200 && this.status < 400){
                // Success!
                successFn(JSON.parse(this.responseText));
            } else {
                // Error :(
                errorFn();
            }
        }
    };

    request.send();
    request = null;
};

module.exports = {
    getJSON: ajaxRequest.bind(ajaxRequest, 'GET'),
    postJSON: ajaxRequest.bind(ajaxRequest, 'POST')
};

/**
 * @type {EventEmitter}
 */
var EventEmitter = require('./EventEmitter');

/**
 *
 * @type {fastdom}
 */
var fastdom = require('fastdom');

/**
 *
 * @param element {HTMlElement}
 * @param configuration {Object}
 * @constructor
 */
var ComponentAbstract = function(element, configuration) {
    this._element = element;
    this._configuration = configuration || {};

    fastdom.read(this._detectElements.bind(this));
    fastdom.write(this._bindEventHandlers.bind(this));
};

// extend EventEmitter
ComponentAbstract.prototype = Object.create(EventEmitter.prototype);
ComponentAbstract.prototype.constructor = ComponentAbstract;

Object.assign(ComponentAbstract.prototype, {
    /**
     * Create internal references to Elements during read cycle
     * @protected
     */
    _detectElements: function () {},

    /**
     * Bind event handlers to components during write cycle
     * @protected
     */
    _bindEventHandlers: function () {}
});

/**
 * @type {ComponentAbstract}
 */
module.exports = ComponentAbstract;

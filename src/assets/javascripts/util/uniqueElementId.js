var uniqueNumber = require('./uniqueNumber');

module.exports = function () {
    var elementId;

    do {
        elementId = 'element' + uniqueNumber();
    } while( document.getElementById(elementId) );

    return elementId;
};
/**
 * Decides which node is the common ancestor of the given nodes
 * in the arguments
 *
 * @param {HTMLElement} node1
 * @returns {*}
 */
module.exports = function(node1 /*, node2, node3, ... nodeN */) {
	if (arguments.length < 2)
		throw new Error("getCommonAncestor: not enough parameters");

	var i,
		method = "contains" in node1 ? "contains" : "compareDocumentPosition",
		test   = method === "contains" ? 1 : 0x0010,
		nodes  = [].slice.call(arguments, 1);

	rocking:
		while (node1 = node1.parentNode) {
			i = nodes.length;
			while (i--) {
				if ((node1[method](nodes[i]) & test) !== test)
					continue rocking;
			}
			return node1;
		}

	return null;
};

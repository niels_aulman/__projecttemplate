/**
 * The purpose of this document is providing a reusable event emitter
 * for prototype object. This allows to add, remove and trigger events.
 *
 * @class
 */
var EventEmitter = function () {};

EventEmitter.prototype = {
    /**
     * Add event listener
     *
     * @param event
     * @param callbackFn
     */
    addEventListener: function (event, callbackFn) {
        this._eventListener = this._eventListener || {};
        this._eventListener[event] = this._eventListener[event] || [];
        this._eventListener[event].push(callbackFn);
    },

    /**
     * Remove event listener
     *
     * @param event
     * @param callbackFn
     */
    removeEventListener: function (event, callbackFn) {
        this._eventListener = this._eventListener || {};

        if (event in this._eventListener !== false) {
            this._eventListener[event].splice(this._eventListener[event].indexOf(callbackFn), 1);
        }
    },

    /**
     * Trigger event on object.
     *
     * @param event
     */
    triggerEvent: function(event /* , args... */){
        this._eventListener = this._eventListener || {};

        if (event in this._eventListener !== false  ) {
            for(var i = 0; i < this._eventListener[event].length; i++){
                this._eventListener[event][i].apply(this, Array.prototype.slice.call(arguments, 1));
            }
        }
    }
};

module.exports = EventEmitter;

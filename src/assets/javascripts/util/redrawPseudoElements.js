/**
 * Forces a redraw on all pseudo elements on the page.
 * This can be used to force IE8 to render icons properly.
 */

var redrawClassName = 'ie-pseudo-redraw';
var styleElement;
var ensureStyleElement = function(){
    if (!styleElement){
        var css = '' +
            'html.' + redrawClassName + ' :before,'+
            'html.' + redrawClassName + ' :after {'+
                'content : none !important;'+
            '}';
        var head = document.head || document.getElementsByTagName('head')[0];
        var style = document.createElement('style');

        style.type = 'text/css';
        if (style.styleSheet){
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);

        styleElement = style;
    }
};

var redrawPsuedoElements = function(){
    ensureStyleElement();

    var html = document.documentElement,
        clss = html.className;

    html.className = clss + " " + redrawClassName;

    setTimeout( function(){
        html.className = html.className.replace(redrawClassName, '');
    }, 10 );
};

module.exports = redrawPsuedoElements;

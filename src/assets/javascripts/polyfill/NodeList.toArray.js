var arraySlice = Array.prototype.slice;

var nodeListToArray = function(){
    var result = [];

    try {
        result = slice.call(this);
    } catch (c) {
        for (var resultIndex = 0, resultLength = this.length; resultIndex < resultLength; resultIndex++) {
            result.push(this[resultIndex])
        }
    }

    return result;
};

if ( window.NodeList && NodeList.prototype && !NodeList.prototype.toArray ){
    NodeList.prototype.toArray = nodeListToArray;
}

if ( window.StaticNodeList && StaticNodeList.prototype && !StaticNodeList.prototype.toArray ){
    StaticNodeList.prototype.toArray = nodeListToArray;
}

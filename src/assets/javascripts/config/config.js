module.exports = (function()){
	var _init = function(){
		_setNameSpace();
		if(global.hasOwnProperty('__PROJECTNAME__')) _setSettings();	
	}

	var _setNameSpace = function(){
		if(!global.hasOwnProperty('__PROJECTNAME__')){
			var __PROJECTNAME__ = {};
			global.__PROJECTNAME__ = __PROJECTNAME__;
		}
	}

	var _setSettings = function(){
		__PROJECTNAME__.Settings = {
			Autoload : true,
			Breakpoints: {
				small: 1,
				medium: 2,
				large: 3,
				extralarge: 4
			},
			Resized: false
		}
	}

	init();
});


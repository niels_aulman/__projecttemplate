'use strict';

// Include Gulp & Tools We'll Use
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var rimraf = require('rimraf');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var path = require('path');
var reload = browserSync.reload;
var fs = require('fs');
var yargs = require('yargs').argv;
var tunnel = yargs.tunnel ? yargs.tunnel : false;
var transformTools = require('browserify-transform-tools');

// plugin for browserify to put certain files winside a scope when initializing
var scopeify = transformTools.makeStringTransform('scopeify', {jsFilesOnly: true}, function (content, transformOptions, done) {
    var file = transformOptions.file;

    if (!transformOptions.config) {
        return done(new Error('Could not find scopeify configuration.'));
    }

    for (var key in transformOptions.config) {
        if (transformOptions.configData.configDir + '/' + key === file) {
            content = [
                '(function(){',
                content,
                '}).call(' + transformOptions.config[key] + ');'
            ].join('');
            break;
        }
    }

    done(null, content);
});


yargs.ftpDeploy = yargs.ftpDeploy || {};


var config = {
    path: {
        src: {
            root: 'src',
            asset: {
                javascript: 'src/assets/javascripts',
                image: 'src/assets/images',
                font: 'src/assets/fonts',
                icon: 'src/assets/fonts/icons',
                scss: 'src/assets/scss'
            },
            prototype: {
                template: 'src/prototype/template',
                webroot: 'src/prototype/webroot'
            },
            patternLibrary: {
                root: 'src/styleguide',
                template: 'src/styleguide/template'
            }
        },

        build: {
            root: 'build',
            asset: {
                javascript: 'build/assets/js',
                image: 'build/assets/img',
                font: 'build/assets/fonts',
                icon: 'build/assets/fonts/icons',
                css: 'build/assets/css'
            },
            prototype: {
                template: 'build/',
                webroot: 'build/cms-assets'
            },
            patternLibrary: {
                root: 'build/styleguide'
            },
            site: 'build/'
        },
        buildUrl: {
            site: '/',
            css: '/assets/css',
            javascript: '/assets/js',
            image: '/assets/img',
            font: '/assets/fonts'
        }
    },
    autoprefix: {
        support: 'last 2 versions, Explorer >= 8, Firefox >= 25'
    },
    browsers: 'google chrome, firefox, safari',
    ftpDeploy: {
        host: yargs.ftpDeploy.host || '',
        password: yargs.ftpDeploy.password || '',
        username: yargs.ftpDeploy.username || ''
    },
    deploy: false
};

// Error reporting for plumber
var onError = function (err) {
    $.util.beep();
        //.pipe($.util.log.colors.white.bgRed.bold(
        //    err
        //));
    console.log(err);
};

// font url relative to css location
/*
 config.path.buildUrl.fontCssRelative = path.relative(
 config.path.buildUrl.css,
 config.path.buildUrl.font
 ).replace(/\\/g, '/');
 */


// image url relative to css location
/*
 config.path.buildUrl.imageCssRelative = path.relative(
 config.path.buildUrl.css,
 config.path.buildUrl.image
 ).replace(/\\/g, '/');
 */

gulp.task('set:deploy',function(){
    config.deploy = true;
});

// build all
gulp.task('build', [
    'build:asset:icon',
    'build:asset',
    'build:prototype',
    'build:pattern_library'
]);

// build for development
// skips some tasks like fonts, icons and patternlibrary
// to make it faster
gulp.task('build:development', [
    'build:asset:scss',
    'build:asset:image',
    'build:asset:javascript',
    'build:prototype'
]);

// build assets
gulp.task('build:asset', [
    'build:asset:font',
    'build:asset:image',
    'build:asset:javascript',
    'build:asset:scss'
]);

// build regular fonts
gulp.task('build:asset:font', function () {
    return gulp.src([config.path.src.asset.font + '/**/*',
        '!' + config.path.src.asset.font + '/icons/*'
    ])
        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe(gulp.dest(config.path.build.asset.font))
        .pipe($.size({title: 'fonts'}));
});

// build icon font
// build icon font
gulp.task('build:asset:icon', function(){

    return gulp.src([config.path.src.asset.icon + '/*.svg'])
        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe($.rename(function (path){

            var pattern = /[0-9]{0,9999}-/;
            var placeholder = /placeholder/;

            var filesystem = require("fs");
            var oldPath;
            var results = [];

            filesystem.readdirSync(config.path.src.asset.icon).forEach(function(file) {
                if(file.split('.').pop() === 'svg'){
                    if(pattern.test(file) && !placeholder.test(file)){
                        results.push(file);
                    }
                }
            });
            if(results.length > 1) results = results.pop();
            var results = String(results).split('-');
            var lastNumber = parseFloat(results[0]) + 1;

            if(!pattern.test(path.basename)){
                var oldPath = '/'+ path.basename + path.extname;
                path.basename = (lastNumber < 10 ) ? '0'+lastNumber + '-' + path.basename : lastNumber + '-' + path.basename;
            }
            if(oldPath) filesystem.unlinkSync(config.path.src.asset.icon + oldPath);
        }))
        .pipe(gulp.dest(config.path.src.asset.icon))
        .pipe($.iconfontCss({
            fontName: 'custom-icons'
        }))
        .pipe($.iconfont({
            fontName: 'custom-icons',
            normalize: true,
            centerHorizontally: true,
            fixedWidth: true
        }))
        .on('codepoints', function(codepoints, options){
            // remove number-prefixes from icons
            var pattern = /[0-9]{0,9999}-/;
            for (var i = codepoints.length - 1; i >= 0; i--) {
                var tmpName = codepoints[i].name;
                if(pattern.test(tmpName)){
                    codepoints[i].name = tmpName.replace(pattern, '');
                }
            }
            return gulp.src(config.path.src.asset.icon + '/font-template.scss')
                .pipe($.plumber({
                    errorHandler: onError
                }))
                .pipe($.consolidate('lodash',{
                    glyphs: codepoints,
                    fontName: options.fontName,
                    fontPath: '../fonts/icons/',
                    prefix: 'icon-',
                    iconClass: 'icon'
                }))
                .pipe($.rename('_icon-font.scss'))
                .pipe(gulp.dest(config.path.src.asset.scss + '/base/project'));
        })
        .pipe(gulp.dest(config.path.build.asset.icon));
});


// Optimise image task
gulp.task('build:asset:image', function () {
    return gulp.src(config.path.src.asset.image + '/**/*')
        // optimizes image file if possible
        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(config.path.build.asset.image))
        .pipe($.size({title: 'asset:image'}));
});

gulp.task('build:asset:javascript', function () {
    return gulp.src([config.path.src.asset.javascript + '/*.js'])
        // use browserify to manage dependencies
        // debowerfy transform is used to allow bower packages to be used
        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe($.if(!config.deploy, $.sourcemaps.init()))
        .pipe($.browserify({
           transform: ['debowerify', scopeify],
           debug: true
        }))
        .pipe($.if(config.deploy, $.uglify()))
        .pipe($.if(!config.deploy, $.sourcemaps.write('./')))
        .pipe(gulp.dest(config.path.build.asset.javascript))
        .pipe($.size({title: 'asset:javascript'}));
});

gulp.task('build:asset:scss', function () {
    // transform url configuration into sass variable format,
    // this allows sass to use configured url naming scheme
    // set in this file
    var sassUrlConfig = '';
    for (var buildUrlKey in config.path.buildUrl) {
        var buildUrlValue = config.path.buildUrl[buildUrlKey];
        sassUrlConfig += '$url-' + buildUrlKey + ': "' + buildUrlValue + '";' + '\n';
    }

    return gulp.src([config.path.src.asset.scss + '/**/**/*.scss'])
        // Prepends sass url configuration
        .pipe($.insert.prepend(sassUrlConfig))

        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe($.if(!config.deploy, $.sourcemaps.init()))
        .pipe($.sass({
            errLogToConsole: config.deploy ? false : true,
            trace: config.deploy ? false : true,
            style: config.deploy ? 'compressed' : 'expanded',
            lineNumber: config.deploy ? false : true
        }))

        .pipe($.autoprefixer(
            config.autoprefix.support.split(', ')
        ))
        .pipe($.if(config.deploy, $.minifyCss()))
        .pipe($.if(!config.deploy, $.sourcemaps.write('./')))
        .pipe(gulp.dest('build/assets/css'))
        .pipe($.size({title: 'styles:scss'}))
        .pipe(
        reload({
                stream:true
            }
        ));
});

// Prototoype site
gulp.task('build:prototype', [
    'build:prototype:template',
    'build:prototype:webroot'
]);

// Compile HTML templates with gulp-swig
gulp.task('build:prototype:template', function () {
    return gulp.src(config.path.src.prototype.template + '/*.html')
        .pipe($.plumber({
            errorHandler: onError
        }))
        .pipe($.swig({
            defaults: {
                cache: false
            },
            data: {
                config: config
            }
        }))
        .on('error', function (error) {
            console.log(error);
        })
        .pipe($.htmltidy({
            doctype: 'html5',
            hideComments: config.deploy ? true : false,
            'fix-bad-comments': false,
            'drop-empty-elements': false,
            wrap: 0,
            indent: true,
            'indent-spaces': 4
        }))
        .pipe(gulp.dest(config.path.build.prototype.template))
        .pipe($.size({title: 'site:html'}));
});

gulp.task('build:prototype:webroot', function () {
    return gulp.src(config.path.src.prototype.webroot + '/**/*')
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(config.path.build.prototype.webroot))
        .pipe($.size({title: 'prototype:webroot'}));
});

// Build pattern library via kss
gulp.task('build:pattern_library', [
    'build:pattern_library:template'
]);

// Generate pattern library with templates
gulp.task('build:pattern_library:template', function () {
    var markupData;

    gulp.src(config.path.src.patternLibrary.template + '/codeSample.html')
        .pipe(gulp.dest(config.path.build.patternLibrary.root));

    return gulp.src([config.path.src.asset.scss + '/**/*.scss'])
        .pipe($.replace(/Markup:.*/ig, function (markupInput) {
            var markupOutput = markupInput;
            var markupValue = markupInput.substr(7).trim();
            var os = require('os');

            // if there is a space in the value of markupValue it is
            // presumed this is not a filename. The markupValue always
            // has to end in .html to make sure its a template.
            if (markupValue.indexOf(' ') === -1 && markupValue.substr(-5) === '.html') {
                try {
                    markupData = fs.readFileSync(config.path.src.prototype.template + '/' + markupValue, 'utf8');

                    markupData = markupData
                        .split(os.EOL)
                        // KSS will think that a markup block is finished after a double
                        // line break, this is prevented by replacing double line breaks
                        // by a single line break.
                        .filter(function (line) {
                            return line.trim() !== '';
                        })
                        // make sure file source is added as scss comment
                        // by prepending // before each line
                        .map(function (line) {
                            return '// ' + line;
                        })
                        .join(os.EOL);

                    // replace inclusion string with value of template
                    markupOutput = markupInput.substr(0, 7) + '\n' + markupData;
                } catch (e) {
                    // do nothing if file not exists
                }
            }
            return markupOutput;
        }))
        .pipe($.kss({
            overview: config.path.src.patternLibrary.root + '/Readme.md',
            templateDirectory: path.resolve(config.path.src.patternLibrary.template)
        }))
        .pipe(gulp.dest(config.path.build.patternLibrary.root))
        .pipe($.size({title: 'styles:pattern_library'}));
});

// Todo document what this does exactly and why
gulp.task('build:pattern_library:jsdoc', function () {
    return gulp.src([
        config.path.src.asset.javascript + '/**/*.js',
        '!' + config.path.src.asset.javascript + '/polyfill/*.js'
    ])
        .pipe($.jsdoc.parser())
        .pipe($.jsdoc.generator(config.path.build.patternLibrary.root + '/jsdoc', {
            path: 'ink-docstrap',
            systemName: 'Something',
            footer: 'Something',
            copyright: 'Something',
            navType: 'vertical',
            theme: 'journal',
            linenums: true,
            collapseSymbols: false,
            inverseNav: false
        }));
});


// Watch Files For Changes & Reload
gulp.task('build:live', function () {
    browserSync({
        server: {
            baseDir: [config.path.build.root]
        },
        notify: false,
        tunnel: tunnel,
        startPath: '/'
    });

    // trigger build action per asset type on change
    gulp.watch([config.path.src.asset.scss + '/**/*.scss'], ['build:asset:scss']);
    gulp.watch([config.path.src.asset.image + '/**/*'], ['build:asset:image']);
    gulp.watch([config.path.src.asset.font + '/**/*'], ['build:asset:font']);
    gulp.watch([config.path.src.asset.javascript + '/**/*.js'], [
        'build:asset:javascript'
        //'analyze:jshint' // also show if javascript plays nice with coding standards
    ]);
    //
    gulp.watch([config.path.src.prototype.template + '/**/*'], ['build:prototype:template']);

    // when build folder changes content, reload browser
    // has a timeout of 60 ms preventing a reload overflow
    gulp.watch([config.path.build.root + '/**/*'], (function () {
        var cb;
        return function () {
            clearTimeout(cb);
            cb = setTimeout(reload, 60);
        };
    })());
});

// Watch Files For Changes & Reload
gulp.task('build:test', function () {
    browserSync({
        server: {
            baseDir: [config.path.build.root]
        },
        notify: false,
        tunnel: true,
        startPath: '/',
        ghostMode: {
            clicks: true,
            forms: true,
            scroll: true
        },
        browser: config.browsers.split(', ')
    });
});



// Analyze tasks, provide information about code quality
gulp.task('analyze', [
    'analyze:jshint',
    'analyze:scsslint'
]);

gulp.task('analyze:jshint', function () {
    gulp.src([
        './*.js',
        config.path.src.root + '/**/*.js',
        '!' + config.path.src.asset.javascript + '/vendor/*',
        '!' + config.path.src.patternLibrary.template + '/**/*',
        '!' + config.path.src.asset.javascript + '/polyfill/*.js'
    ])
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'));
});

gulp.task('analyze:scsslint', function () {
    // lint scss for parsing errors
    gulp.src([
        config.path.src.asset.scss + '/components/*.scss',
        config.path.src.asset.scss + '/base/project/*.scss',
        config.path.src.asset.scss + '/specifics/**/*.scss',
        config.path.src.asset.scss + '/site.scss'
    ])
        .pipe($.scssLint({
            'config': '.scss-lint.yml'
        }));
});


// Clean Output Directories
gulp.task('clean', function (cb) {
    rimraf(config.path.build.root, cb);
});

// Build and start development server
gulp.task('develop', function (cb) {
    runSequence('clean', 'build', ['build:live' , 'build:pattern_library'], cb);
});

gulp.task('test', function (cb) {
    runSequence('clean', 'build', ['build:test',  'build:pattern_library'], cb);
});

gulp.task('deploy', function (cb) {
    runSequence('set:deploy', 'clean', 'build', cb);
});

// Default is alias for build
gulp.task('default', ['build']);

//deploy task
gulp.task('deploy:ftp', ['clean', 'build'], function () {
    return gulp.src(config.path.build.root + '/**/*')
        .pipe($.ftp({
            host: config.ftpDeploy.host,
            user: config.ftpDeploy.username,
            pass: config.ftpDeploy.password
        }));
});
